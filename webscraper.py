import requests
import bs4
import os, random

url_base = 'http://brage.bibsys.no'
thesis_end = '?show=full'
items_per_page = 10
page_number = 0
document_type = 'Master+thesis'
output_folder = os.getcwd() + '\Output\\'
full_url = url_base + '/xmlui/browse?order=ASC&rpp={}&sort_by=-1&value={}&etal=-1&offset={}&type=type'.format(items_per_page, document_type, items_per_page*page_number)
print(full_url)
full_html = requests.get(full_url)
full_page = bs4.BeautifulSoup(full_html.text, "lxml")


def get_thesis_links(browse_page):
    return [a.attrs.get('href') for a in browse_page.select('div.artifact-title a[href^=/xmlui/handle/11250]')]


def get_attributes(thesis_page):
    attributes = [attribute.contents[0] for attribute in thesis_page.select('td.label-cell')]
    values = [get_content_from_id(thesis_page, attr) for attr in attributes]
    d = dict(zip(attributes, values))
    if not d['dc.identifier.uri'].startsWith('http://hdl.handle.net'):
        print("does not start with...")
        try:
            d['dc.identifier.uri'] = thesis_page.find_all(text='dc.identifier.uri')[1].parent.findNext('td').text
        except Exception as e:
            print("Failed to find alternative link: " + e)
    return d


def request_thesis(thesis_page):
    html = requests.get(url_base + thesis_page + thesis_end)
    return bs4.BeautifulSoup(html.text, 'lxml')


def get_content_from_id(soup, identifier):
    return soup.find(text=identifier).parent.findNext('td').text


def get_institution(soup):
    return soup.find(id='ds-trail').contents[5].a.text


def write_to_file(attributes):
    directory_exists(output_folder)
    title = attributes['dc.identifier.uri'].split('/')[-1] + ".txt"
    try:
        f = open(output_folder + title, "w", encoding='utf-8')
    except Exception as e:
        f = open(output_folder + str(random.randrange(1, 99000, 1)) + '.txt', "w", encoding='utf-8')
        print(e)
    try:
        f.write(attributes['dc.description.abstract'])
    except Exception as writeError:
        f.write('')
        print(writeError)
    f.close()


def directory_exists(p):
    if not os.path.exists(p):
        os.makedirs(p)


def main():
        all_thesis = [request_thesis(page) for page in get_thesis_links(full_page)]
        thesis_attributes = [get_attributes(thesis) for thesis in all_thesis]
        [write_to_file(thesis) for thesis in thesis_attributes]

if __name__ == '__main__':
    main()

