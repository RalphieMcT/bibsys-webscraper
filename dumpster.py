import os

url_base = 'brage.bibsys.no/xmlui/'
items_per_page = 20
page_number = 2
document_type = 'Master+thesis'
full_url = url_base + 'browse?order=ASC&rpp={}&sort_by=-1&value={}&etal=-1&offset={}&type=type'.format(items_per_page, document_type, items_per_page*page_number)
